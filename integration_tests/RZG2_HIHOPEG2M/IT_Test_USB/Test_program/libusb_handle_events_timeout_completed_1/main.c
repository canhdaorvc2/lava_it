/*
* Project: CIP LAVA IT test
* Test ID: libusb_handle_events_timeout_completed_1
* Feature: Checking handle_events_timeout_completed system call
* Sequence: libusb_init();libusb_set_debug();libusb_get_device_list();libusb_get_device_descriptor();libusb_get_config_descriptor();libusb_free_config_descriptor();libusb_open_device_with_vid_pid();libusb_free_device_list();libusb_claim_interface();libusb_bulk_transfer();libusb_release_interface()
* Testing level: system call
* Test-case type: Normal
* Expected result: OK
* Name: main.c
* Author: RVC/AnhTran (anh.tran.jc@rvc.renesas.com)
* Version: v00r01
* Copyright: Renesas
* Target board: Ebisu_EK874_HIHOPEG2M_HIHOPEG2N
* Details_description: External device: USB Storage. Condition: Call API libusb_handle_events_timeout_completed of usblib library. Expected result = OK
*/
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <libusb.h>
#include <sys/types.h>
#include <stdlib.h>

void segfault_sigaction(int signal, siginfo_t *si, void *arg);

int main(int argc, char *argv[])
{
	int result;
	struct libusb_context *ctx;

	struct timeval tv;
	tv.tv_sec = 3;
	tv.tv_usec = 0;

	//Segmentation fault control
	struct sigaction sa;
	memset(&sa, 0, sizeof(sigaction));
	sigemptyset(&sa.sa_mask);
	sa.sa_sigaction = segfault_sigaction;
	sa.sa_flags = SA_SIGINFO;
	sigaction(SIGSEGV, &sa, NULL);

	//Main checking item
	libusb_init(&ctx);
	if (result < 0) {
		printf("Error: libusb_init");
		return 0;
	}

	result = libusb_handle_events_timeout_completed(ctx, &tv, NULL); // Test function with timeout 3s
									 // Expected value 0
	libusb_exit(ctx);

	if( result == 0 ) {
		printf ("OK\n");
	} else {
		printf ("NG\n");
	}

	return 0;
}

void segfault_sigaction(int signal, siginfo_t *si, void *arg)
{
	printf("NG_SF\n");
	exit(0);
}


