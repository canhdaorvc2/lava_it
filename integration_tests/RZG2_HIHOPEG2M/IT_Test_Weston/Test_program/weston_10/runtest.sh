#!/bin/bash
#get the current location - test case name
#Writer: RVC/CanhDao
tc_dir=$1
echo "[test_start:test_case]"
./main &

sleep 2

#task_pid=`ps | egrep main | egrep -v "egrep"  | awk '{print $1}'`
task_pid=`ps aux| egrep main | awk '{print $1}'`
#task_pid=12
echo "ok"
echo $task_pid
if [ $task_pid != "0" ]
then
        echo "$tc_dir+pass" >> ../../log_file.txt #"+" is a separator will be used in send-to-lava.sh
        kill $task_pid
        exit 0
else
        echo "$tc_dir+fail+[test_log:TPREL_test_case_$result:Expected value mismatch with Real value. OK vs $result]" >> ../../log_file.txt
        exit 1
fi
#echo $task_pid
~         
